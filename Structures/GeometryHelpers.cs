﻿namespace _01_Structures
{
    public static class GeometryHelpers
    {
        public static void DisplayCoordinates(CoordinatesIn2D point)
        {
            Console.WriteLine($"Point is on coordinates x: {point.X}, y: {point.Y}");
        }
    }
}

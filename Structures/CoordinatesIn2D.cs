namespace _01_Structures
{
    /*
     * Toto je struktura. 
     * Umožňuje nám svázat libovolné množství proměnných do logického celku. 
     * Můžeme se na ni dívat jako na nový datový typ.
     * 
     * V tomto případě jsme vytvořili nový datový typ pro reprezentaci bodu v 2D prostoru.
     * 
     *          === Každý nový datový typ (včetně struktur) bude ve svém vlastním souboru ===
     *          
     * Jinými slovy nebudeme spamovat Program.cs definicí vlastních datových typů.
     * Nyní se vraťte do Program.cs (můžete kliknout na "X references" nad "public struct")
     */
    public struct CoordinatesIn2D
    {
        public int X; 
        public int Y;
    }
}

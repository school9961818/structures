using _01_Structures;

/* 
 * Uvažujme jednoduchý bod v dvojrozměrném prostoru:
 */

int x = 1;
int y = 2;
Console.WriteLine($"First point is on coordinates x: {x}, y: {y}");

/*
 * Vytvořte nový bod v 2D prostoru a vypište jeho souřadnice na obrazovku.
 */


/*
 * Jaký název proměnných jste použili? x1, y1? a, b? Jinak?
 * A jaké názvy byste použili pro reprezentaci desítek bodů?
 * Nebo grafického modelu z tisíce trojúhelníků?
 * 
 * 
 * Další výzvou ve stále větších a komplexnějších programech byla nemožnost reprezentace složitějších objektů.
 * Jedno číslo může reprezentovat věk, výplatu nebo známku, ale věci jako atmosferické podmínky, 
 * vektorové veličiny nebo souřadnice v prostoru už s pomocí základních proměnných nebylo snadné vyjádřit.
 *
 * Struktury vznikly jako odpověď na tyto výzvy.
 * 
 * Klikněte na "CoordinatesIn2D" a dejte F12 (nebo pravý click a "go to definition")
 */

CoordinatesIn2D pointIn2D;







/*
 * Když máme strukturu vytvořenou, můžeme přistupovat k jejím proměnným.
 * Těm říkáme "property" a začínáme je velkým písmenem.
 */
CoordinatesIn2D firstPoint;
firstPoint.X = 10;
firstPoint.Y = 15;

/*
 * Můžeme to také zkrátit do jednoho příkazu (podobně jako se základními proměnnými)
 * V takovém případě ale stejně doporučím ten příkaz následovně odřádkovat.
 */
CoordinatesIn2D secondPoint = new CoordinatesIn2D()
{
    X = 30,
    Y = 30
};

/*
 * Jakmile máme strukturu vytvořenou, pracujeme s ní jako s jakoukoliv jinou proměnnou.
 */
/*
GeometryHelpers.DisplayCoordinates(firstPoint);
GeometryHelpers.DisplayCoordinates(secondPoint);

Console.WriteLine("This should override the value in second point. Let's check...");
secondPoint = firstPoint;
GeometryHelpers.DisplayCoordinates(firstPoint);
GeometryHelpers.DisplayCoordinates(secondPoint);
*/

/*
 * Vytvořte třetí bod a vypište jeho souřadnice na obrazovku. 
 */

/*
 * V souboru GeometryHelpers.cs vytvořte metodu
 *   Vstupní parametr - bod v 2D prostoru (CoordinatesIn2D)
 *   Výstupní parametr - vzdálenost od středu (středem je bod [0,0]) 
 * Na obrazovku vypište výsledek pro druhý a tředí bod 
 */


/*
 * Upravte metodu pro výpočet vzdálenosti tak, aby vzdálenost zaokrouhlovala na 2 desetinná místa.
 *  (Pro druhý bod [10, 15] by měl výsledek být "18.03")
 */

/*
 * Vytvořte novou strukturu pro reprezentaci bodu ve 3D prostoru.
 * Nezapomeňte, že každý nový datový typ musí být ve svém vlastním ".cs" souboru
 * 
 * Vytvořte dvě nové proměnné pro body v 3D prostoru. 
 */


/*
 * Porovnejte a vypište na obrazovku, který z těchto dvou bodů je výš (výšku udává souřadnice Y)
 */


/*
 * V souboru GeometryHelpers.cs vytvořte metodu
 *   Vstupní parametry 
 *     bod v 2D prostoru
 *     bod v 2D prostoru
 *   Výstupní parametr 
 *    vzdálenost mezi body (zaokrouhlená na 2 desetinná místa) 
 * Na obrazovku vypište výsledek pro dva body 
 */

/*
 * V souboru GeometryHelpers.cs vytvořte metodu
 *   Vstupní parametry 
 *     bod v 3D prostoru
 *     bod v 3D prostoru
 *   Výstupní parametr 
 *    vzdálenost mezi body (zaokrouhlená na 2 desetinná místa) 
 * Na obrazovku vypište výsledek pro vaše body 
 */



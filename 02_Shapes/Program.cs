using _01_Structures;
using _02_Shapes;

/*
 * Pokud je struktura datový typ a může mít uvnitř proměnné libovolného datového typu,
 * může mít struktura uvnitř další struktury? 
 * 
 * Ano. Může.
 * Prohlédněte si datový typ "Circle" (F12)
 */

Circle example;




/*
 * Opět si ukážeme dvě možnosti, jak vytvořit strukturu.
 */
CoordinatesIn2D firsCenter;
firsCenter.X = 1;
firsCenter.Y = 1;
Circle firstCircle;
firstCircle.Center = firsCenter;
firstCircle.Radius = 2.3f;

Circle secondCircle = new Circle()
{
    Center = new CoordinatesIn2D()
    {
        X = 2,
        Y = 2
    },
    Radius = 5.1f
};

/*
 * Zkuste se zamyslet (nebo googlit) a vytvořte třetí kruh ještě jiným způsobem.
 * Nebo kombinací dvou předchozích.
 */

/*
 * Vyvořte nový soubor, kam si budete dávat pomocné metody pro práci s geometrickými tvary.
 * Zamyslete se nad pojmenováním.
 */

/*
 * V novém souboru vytvořte metodu
 *   Vstupní parametry 
 *     kruh (Circle)
 *   Výstupní parametr 
 *    obsah kruhu zaokrouhlený na 2 desetinná místa 
 * Na obrazovku vypište výsledek pro všechny tři kruhy (první by měl být 16.62)
 */

/*
 * V novém souboru vytvořte metodu, která určí jestli se kruhy kompletně překrývají (např: https://vt-vtwa-assets.varsitytutors.com/vt-vtwa/uploads/problem_question_image/image/2895/Figure.gif)
 *   Vstupní parametry 
 *     kruh (Circle)
 *     kruh (Circle)
 *   Výstupní parametr 
 *    true nebo false 
 * Na obrazovku vypište výsledek pro kombinaci všech tří kruhů (pro první a druhý by měl být výsledek True)
 *   Hint: Známe poloměry a máme metodu pro vzdálenost dvou bodů (viz předchozí cvičení)
 */

/*
 * V novém souboru vytvořte metodu, která určí jestli se kruhy vůbec nedotýkají. (např: https://ecdn.teacherspayteachers.com/thumbitem/Two-Circles-Paper-Landscape-4388633-1675297251/original-4388633-1.jpg)
 * Nemají žádný společný bod.
 *   Vstupní parametry 
 *     kruh (Circle)
 *     kruh (Circle)
 *   Výstupní parametr 
 *    true nebo false 
 * Na obrazovku vypište výsledek pro kombinaci všech tří kruhů (pro první a druhý by měl být výsledek False)
 */

/*
 * V novém souboru vytvořte metodu, která určí jestli se kruhy částečně překrývají 
 *   Vstupní parametry 
 *     kruh (Circle)
 *     kruh (Circle)
 *   Výstupní parametr 
 *    true nebo false 
 * Na obrazovku vypište výsledek pro kombinaci všech tří kruhů (pro první a druhý by měl být výsledek False)
 *   Hint: Když vezmeme dva kruhy, a vyloučíme že se překrývají kompletně, pak vyloučíme, že se nedotýkají, jaké kruhy nám zbydou?
 */

/*
 * Vytvořte novou strukturu (v novém souboru) pro reprezentaci trojúhelníku.
 * Trojůhelník bude mít tři body (CoordintesIn2D)
 * Vytvořte proměnnou tohoto typu.
 */

/*
 * V souboru pro pomocné metody vytvořte metodu, která určí jestli je trojúhelník validní
 * Validní trojůhelník má nějaký obsah
 *   Vstupní parametry 
 *     trojúhelník
 *   Výstupní parametr 
 *    true nebo false 
 * Na obrazovku vypište výsledek pro Váš trojúhelník.
 *   Hint: 
 *     [1,1][1,1][1,1] není validní trojúhleník (je to třikrát stejný bod)
 *     [1,1][2,2][3,3] není validní trojúhleník (je to přímka)
 */


/*
 * V souboru pro pomocné metody vytvořte metodu, která vytvoří náhodný bod v 2D prostoru
 * Bod bude mít X i Y sou5adnici v rozmezí [0,10]
 *   Vstupní parametry 
 *     žádné
 *   Výstupní parametr 
 *    CoordinatesIn2D
 * Vytvořte nový bod s pomocí této metody a vypište jeho souřadnici na obrazovku.
 */

 /*
 * Upravte rádius třetího kruhu tak, aby měl s druhým kruhem částečný překryv.
 */

/*
 * V souboru pro pomocné metody vytvořte metodu, která vytvoří náhodný trojúhelník s pomocí metody pro náhodné body
 *   Vstupní parametry 
 *     žádné
 *   Výstupní parametr 
 *    trojúhelník
 * V cyklu vytvořte 20 nových trojúhelníků a pro každý vypište jeho body a jestli se jedná o validní nebo nevalidní trojúhelník.
 */

 /*
 * V souboru pro pomocné metody vytvořte metodu
 *   Vstupní parametry 
 *     triangle
 *     circle
 *   Výstupní parametr 
 *    bool
 * Metoda určí, zda je trojúhelník celý obsažen v kruhu.
 * Vygenerujte dvacet náhodných trojúhelníků a určete, zda jsou uvnitř druhého kruhu 
 */

/*
 * BONUS: Upravte veškerá řešení v tomto cvičení tak, aby zvládala i záporná čísla.
 *  Aby body byly [-10, 10] a aby vše fungovalo správně
 */

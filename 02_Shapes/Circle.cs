﻿using _01_Structures;

namespace _02_Shapes
{
    /*
     * Kruh můžeme definovat např. jeho středem a poloměrem.
     * Všimněte si, že pro reprezentaci středu jsme použili datový typ z prvního projektu.
     * To je možné a žádoucí. 
     * Celá struktura řešení (Solution) by měla sloužit ke zpřehlednění a logického uspořádání projektu.
     * Podobně jako adresáře na disku, aby v tom nebyl chaos.
     * Nyní se vraťte do Program.cs
     */
    public struct Circle
    {
        public CoordinatesIn2D Center;
        public float Radius;
    }
}

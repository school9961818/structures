### Prerequisites

[Methods](https://gitlab.com/school9961818/methods)

### Goal

* Code
  * Struct
* VisualStudio
  * Create a file

### Diffuculty
* Begginers
* 3-5x45m

### Insight

Nejžádanější vlastností programátora je **samostatnost**. 

Tedy schopnost zeptat se, najít si informace a nějak si poradit. Je to z důvodu, že pokud firma už nějaké programátory má, primárně chce, aby se věnovali programování. 

Zaučování nováčků firmě nic nepřináší, a pokud takový nováček třeba po zkušebce odejde, pak je to extrémně drahá a nevydařená investice. 

Častý problém úspěšných startupů je expanze. Když ke třem vývojářům najednou naberete pět nových, co se asi stane? Kdo se jim bude věnovat a kdo zbyde na práci?

Téměř každý IT pohovor obsahuje nějakou formu otázky "A jak budete postupovat, když se zaseknete?". Případně dostanete na místě obskurního úkol nebo dotaz (který nikdo nemůže vědět z hlavy) a k tomu erární notebook s internetem. Oboje má jediný cíl - zjistit, jak si dovedete poradit.

Samostatnost často přebíjí senioritu, znalost technologie, platové požadavky nebo zkušenosti z oboru. Protože to vše se dá snadno dohnat (zvlášť, pokud je člověk samostaný).

Nebojte se ptát. Naučte se správně ptát. Čím častěji to budete dělat, tím efektivnější v tom budete. Čím efektivnější budete v řešení problémů, tím budete samostatnější.

Pokud si nejste jistí jak na to, můžete se inspirovat třeba následujícím flowchartem
```mermaid
flowchart TD
    A(Problém) ---> LLM
    LLM(Zkusím řešit s AI. Případně google. Dám tomu deset minut.)
    LLM  ---> Cond1
    Cond1{Pohnul jsem se z místa?}
    Cond1 -- Ano ---> LLM
    Cond1 -- Ne ---> Peer
    Peer(Zeptám se na radu vrstevníka. Např.: jiného studenta)    
    Peer ---> Cond2
    Cond2{Pohnul jsem se z místa?}
    Cond2 -- Ano ---> LLM
    Senior(Zeptám se na radu seniornějšího. Např.: učitele)    
    Cond2 -- Ne ---> Senior
  
```


### Instructions 

Naklonujte si projekt do počítače a postupně vyřešte zadání ve všech projektech.


### Feedback

https://forms.gle/x1KRXgUe8BnuhCBV8

### Where to next

[Testing](https://gitlab.com/school9961818/testing)







